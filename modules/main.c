#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>

#include "engine.h"
#include "tela.h"
#include "pecas.h"

int main(int argc, char const *argv[]) {
  Tela *tela;
  char** tmp;
  int pontuacao;
  int y, x;

  //Inicio do Jogo
  inicializa_ncurses();
  tela = cria_tela();
  nova_peca(tela);
  mostra_tela(tela);
  
  //Logica
  //pega_input( getch );

  //Fim do Jogo
  finaliza_ncurses();
  free(tela->peca);
  free(tela);
  return 0;
}
