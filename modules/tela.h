#ifndef TELA_MOD
#define TELA_MOD

#define LIMITE_X 15
#define LIMITE_Y 25
#define LIMITE_SUPERIOR 5

typedef struct peca Peca;

struct tela{
  char caixa[LIMITE_Y][LIMITE_X];
  Peca* peca;
};

typedef struct tela Tela;

Tela* cria_tela(void);

void mostra_tela (Tela* t);

#endif //TELA_MOD
