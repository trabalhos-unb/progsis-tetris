#include <ncurses.h>
#include "engine.h"

void inicializa_ncurses(void){
  initscr(); // Inicializa a tela
  noecho(); // Evita impressa de teclas pressionadas na tela
  cbreak();
  curs_set(FALSE); // Nao mostra cursor piscando na tela
  keypad(stdscr, TRUE);
}

void finaliza_ncurses(void){
  endwin();
}

int pega_input(int input){
  //Utiliza getch da ncurses para obter valores
  switch( input ){
    case KEY_LEFT :
      //Retorna valor correspondente a tecla esquerda
      return KEY_LEFT;
      break;
    case KEY_RIGHT :
      //Retorna valor correspondente a tecla direita
      return KEY_RIGHT;
      break;
    case KEY_DOWN :
      //Retorna valor correspondente a tecla baixo
      return KEY_DOWN;
      break;
    default:
      return -1;
      break;
  }
}

//int main(void){return 0;}
