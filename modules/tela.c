#include <stdlib.h>
#include <ncurses.h>
#include "tela.h"

Tela* cria_tela(void){
  int y, x;
  Tela* nova_tela = calloc(1, sizeof(Tela) );
  //Cria bordas
  for( y = 0 ; y < LIMITE_Y ; y++ )
    for( x = 0 ; x < LIMITE_X ; x++ ){
      nova_tela->caixa[y][x] = ' ';
      //Se linha == 0
      if( y == 0 ){
        nova_tela->caixa[y][x] = '-';
      }
      //Se linha == 25
      if( y == (LIMITE_Y - 1) ){
        nova_tela->caixa[y][x] = '-';
      }
      //Se for a lateral direita
      if( x == 0 ){
        if( x == LIMITE_SUPERIOR )
          nova_tela->caixa[y][x] = '=';
        else
          nova_tela->caixa[y][x] = '-';
      }
      //Se for a lateral esquerda
      if( x == (LIMITE_X - 1) ){
        if( x == LIMITE_SUPERIOR )
          nova_tela->caixa[y][x] = '=';
        else
          nova_tela->caixa[y][x] = '-';
      }
    }

    return nova_tela;
}

void mostra_tela (Tela* t){
  int y, x;

  for( y = 0 ; y < LIMITE_Y ; y++ ){
    for( x = 0 ; x < LIMITE_X ; x++ ){
      printw("%c\n", t->caixa[y][x]);
    }
      printw("\n");
  }

  //move((t->peca->pos_y - t->peca->tamanho/2), t->peca->pos_x);

}


//int main(void){return 0;}
