#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "pecas.h"

void nova_peca(Tela* tela){
  Peca *nova_peca = calloc(1, sizeof(Peca) );
  char caracter_corpo = ']';
  short int tamanho = 3 + rand()%3, i;

  srand( time(NULL) );
  //Preenche o corpo com o caracter
  for(i = 0; i < tamanho; i++)
    nova_peca->corpo[i] = caracter_corpo;

  if( rand()% 2){
    nova_peca->flag_direcao = VERTICAL;
  }else{
    nova_peca->flag_direcao = HORIZONTAL;
  }
  //A coordenadas x e y, sao utilizadas
  //no mesmo sentido do plano cartesiano
  nova_peca->pos_y = 1; //Comeca um uma posicao após a borda
  nova_peca->pos_x = rand()%(LIMITE_X - 2) + 1;

  tela->peca = nova_peca;

}

void move_peca_x(Peca* peca, int x){
  /*Evitar passagem de valores erroneos*/
  if( x > LIMITE_X)
    x = LIMITE_X - 1;
  else if( x < 1 )
    x = 1;

  peca->pos_x = x;
}

void move_peca_y(Peca* peca, int y){
  /*Evitar passagem de valores erroneos*/
  if( y > LIMITE_Y)
    y = LIMITE_Y - 1;
  else if( y < 1 )
      y = 1;

  peca->pos_y = y;
}

//int main(void){return 0;}
