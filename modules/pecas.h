#include "tela.h"

#ifndef PECAS_MOD
#define PECAS_MOD

#define HORIZONTAL 0
#define VERTICAL 1

struct peca{
    char corpo[6];
    short int pos_x;
    short int pos_y;
    char flag_direcao;
    short int tamanho;
};

void nova_peca(Tela* tela);

void move_peca_x(Peca* peca, int x);

void move_peca_y(Peca* peca, int y);

#endif //PECAS_MOD
