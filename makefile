compiler = clang
nome = tetris.exe
dir = modules/
mode = -std=c99

all: engine.o peca.o tela.o
	$(compiler) $(mode) -o $(nome) *.o $(dir)main.c -lncurses
	rm *.o
	./$(nome)

peca.o:
	$(compiler) $(mode) -c $(dir)pecas.c

tela.o:
	$(compiler) $(mode) -c $(dir)tela.c

engine.o:
	$(compiler) $(mode) -c $(dir)engine.c -lncurses
